/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author Vicente
 */
public class Board extends JPanel {
    
    private Cell cell;
    
    private int numRows;
    private int numCols;
    private int numBombs;
    private int timerDelay;
    private int seconds;
    
    private Timer secondsTimer;
    
    private Cell[][] playBoard;
    
    private class MyMouseAdapter extends MouseAdapter {
        
        public void mousePressed(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                int row = (int) e.getY() / squareHeight();
                int col = (int) e.getX() / squareWidth();
                pointLeftClicked(row, col);
                
            } else if (e.getButton() == MouseEvent.BUTTON3) {
                int row = (int) e.getY() / squareHeight();
                int col = (int) e.getX() / squareWidth();
                pointRightClicked(row, col);
            }
            
            repaint();
        }
        
    }

    public Board() {
        super();
        myInit();
    }
    
    public Board(int numRows, int numCols, int numBoms) {
        super();
        this.numRows = numRows;
        this.numCols = numCols;
        this.numBombs = numBoms;
        myInit();
        
        secondsTimer = new Timer(timerDelay, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                seconds++;
            }
        });
        
    }
    
    private void myInit() {
        playBoard = new Cell[numRows][numCols];
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                playBoard[row][col] = new Cell(row, col, LeftStatus.Void);
            }
        }
        putBombs();
        putNumbers();
        
        timerDelay = 1000;
        seconds = 0;
        repaint();
    }
    
    private void putBombs() {
        while (numBombs > 0) {
            int row = (int) (Math.random() * numRows);
            int col = (int) (Math.random() * numCols);
            if (playBoard[row][col].getCurrentLeftStatus() != LeftStatus.Bomb) {
                cell = new Cell(row, col, LeftStatus.Bomb);
                playBoard[row][col] = cell;
                numBombs--;
            }
        }
    }

    private void putNumbers() {
        int bombsAround = 0;
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                if (playBoard[row][col].getCurrentLeftStatus() != LeftStatus.Bomb) {
                    for (int currentRow = row - 1; currentRow <= row + 1; currentRow++) {
                        for (int currentCol = col - 1; currentCol <= col + 1; currentCol++) {
                            if (pointExist(currentRow, currentCol) && (currentCol != col || currentRow != row)) {
                                bombsAround++;
                            }
                        }
                    }
                    if (bombsAround != 0) {
                        playBoard[row][col] = new Cell(row, col, LeftStatus.Number);
                    }
                }
            }
        }
        
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                Util.drawSquare(g2d, row, col, squareWidth(), squareHeight());
            }
        }
    }
    
    private int squareWidth() {
        if (numCols != 0) {
            return getWidth() / numCols;
        }
        return getWidth() / 100;
    }
    
    private int squareHeight() {
        if (numRows != 0) {
            return getHeight() / numRows;
        }
        return getHeight() / 100;
    }
    
    private boolean pointExist(int currentRow, int currentCol) {
        if (currentRow < 0 || currentRow >= numRows || currentCol < 0 || currentCol >= numCols) {
            return false;
        }
        return true;
    }
    
    private void pointRightClicked(int row, int col) {
        switch (playBoard[row][col].getCurrentRightStatus()) {
            case Flag:
                playBoard[row][col].setCurrentRightStatus(RightStatus.IDK);
                break;
            case IDK:
                playBoard[row][col].setCurrentRightStatus(RightStatus.None);
                break;
            case None:
                playBoard[row][col].setCurrentRightStatus(RightStatus.Flag);
                break;
        }
    }
    
    private void pointLeftClicked(int row, int col) {
        switch (playBoard[row][col].getCurrentLeftStatus()) {
            case Bomb:
                //endGame
                break;
            case Number:
                //RevealNumber
                break;
            case Void:
                //RevealAllVoidNearThis
                break;
        }
    }
    
}
