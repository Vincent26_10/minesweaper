/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper;

/**
 *
 * @author Vicente
 */
public class Cell {
    private int row;
    private int col;
    private LeftStatus currentLeftStatus;
    private RightStatus currentRightStatus;

    public Cell(int row, int col, LeftStatus currentLeftStatus) {
        this.row = row;
        this.col = col;
        this.currentLeftStatus = currentLeftStatus;
        currentRightStatus= RightStatus.None;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public RightStatus getCurrentRightStatus() {
        return currentRightStatus;
    }

    public void setCurrentRightStatus(RightStatus currentRightStatus) {
        this.currentRightStatus = currentRightStatus;
    }
public LeftStatus getCurrentLeftStatus() {
        return currentLeftStatus;
    }
    
}
